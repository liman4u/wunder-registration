<?php

namespace App\Repositories;

use App\Models\Customer;
use Illuminate\Database\QueryException;
use Log;

/**
 * Class CustomerRepository
 * @package App\Repositories
 */
class CustomerRepository
{

    /**
     * CustomerRepository constructor.
     * @param Customer $customer
     */
    public function __construct(Customer $customer)
    {
        $this->model = $customer;
    }
    
    
    /**
     * @param array $data
     * @return Customer
     */
    public function createCustomer(array $data) : Customer
    {

        try {
            return $this->model->create($data);
        } catch (QueryException $e) {
            Log::error($e->getMessage());
            return null;
        }
    }

}