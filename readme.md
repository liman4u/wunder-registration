# Wunder Registration Web App
- A Simple web app for registration of wunder fleet customers

# Technology

- Laravel Framework v5.8

- Docker v19.03.1

- PHP v7.1.3

- Data was persisted with MySql v5.7.22 
    - Connection details:
        - port: `3306`
        - MYSQL_DATABASE: `wunder_db`
        - MYSQL_USER: `root`
        
- Nginx as the reverse proxy server        

- Testing was done with PhpUnit

# Main Packages

- This can be found in the composer.json in the root directory of the project

- PhpUnit 7.5 was used for testing , i am more familiar with this than others like Codeception and Behat


# To run using docker [Recommended]

- Git clone this repository
- Change directory into root of cloned folder
- Rename `.env.docker`  to `.env` (This contains the app configs and databases settings )
- Configure your database settings in `.env` (The DB_HOST needs to be `db`)
- Enter `sh ./start.sh` to start the 3 docker containers ( php , nginx and mysql )
- Open http://localhost or http://[local ipaddress] to view app

# To run locally without docker

- Git clone this repository
- Change directory into root of cloned folder
- Enter `composer install` (assuming you have `composer` and its related packages installed and or configured)
- Rename `.env.example`  to `.env` (This contains the app configs and databases settings)
- Configure your database settings in `.env`
- Enter `php artisan serve` to start application
- Enter `php artisan migrate` to run migration (Dump of database is also found in database/dumps)
- Run tests with `composer test`
- Open ttp://localhost:8000 or http://127.0.0.1:8000 to view app
 
# Database Dump
- This is located in database/dumps/dump-wunder_db-201908280120.sql in the root directory of the project
 
# Data Migration

- This is found in database/migrations/ in the root directory of the project

# Routes

- This can be found in routes/web.php in the root directory of the project 

# Possible Performance Optiomatizations
 
1- Profile Queries - by using Debug Bar , will allow you to see what queries are invoked
 
2- Minify and combine css and javascript -  allows to lower request from website
 
3- Use New Relic - provides deep insight into what is happening in your Laravel application. This will allow you to find memory leaks, slow queries etc.
 
# Things that could have been done better
 
1- Testing frontend with Laravel Dusk 
 

# Screenshots
#### Home Page - Step 1
![Image](screenshots/1.png?raw=true "HomePage")

#### Address Information Page - Step 2
![Image](screenshots/2.png?raw=true "Address Information Page")

#### Payment Information Page - Step 3
![Image](screenshots/3.png?raw=true "Payment Information Page")

#### Confirmation Information Page - Step 4
![Image](screenshots/4.png?raw=true "Confirmation Information Page")

#### Error : Payment Failure
![Image](screenshots/5.png?raw=true "Error : Payment Failure")
