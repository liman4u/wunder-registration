<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'wunder_payment_api' => [
        'url' => env('HPP_API_URL','https://37f32cl571.execute-api.eu-central-1.amazonaws.com/default'),
    ]

];
