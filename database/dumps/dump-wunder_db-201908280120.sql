-- MySQL dump 10.13  Distrib 5.7.26, for Linux (x86_64)
--
-- Host: localhost    Database: wunder_db
-- ------------------------------------------------------
-- Server version	5.7.26-0ubuntu0.19.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `customers`
--

DROP TABLE IF EXISTS `customers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telephone` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `street_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `house_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `zip_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `account_owner` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `IBAN` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payment_status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'PENDING',
  `payment_id` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `customers_telephone_unique` (`telephone`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customers`
--

LOCK TABLES `customers` WRITE;
/*!40000 ALTER TABLE `customers` DISABLE KEYS */;
INSERT INTO `customers` VALUES (13,'Anabel Ziemann','Terrell Hoppe','+1 (449) 757-3762','Ullrich Road','9954 Carissa Wall','Ea.','Port Karinaside','Dejah Mohr PhD','DE43670079188590883188','PENDING',NULL,'2019-08-27 07:27:35','2019-08-27 07:27:35'),(14,'Mekhi Kris Jr.','Dr. Assunta Walter MD','(517) 387-7414','Filomena Square','92121 Lyla Greens','Nesciunt.','Aufderharport','Prof. Ian Reichert IV','DE15111419990547518577','PENDING',NULL,'2019-08-27 07:27:35','2019-08-27 07:27:35'),(15,'Ava Flatley','Clara Baumbach','(306) 629-7902','Marlee Dam','10412 Koepp Hill','Dolores.','Hirtheland','Kathryn Graham DVM','DE04280221517400819317','PENDING',NULL,'2019-08-27 07:27:36','2019-08-27 07:27:36'),(16,'Dr. Randy Cormier','Freddy Dooley','387-514-6020 x9425','Aubrey Square','90646 Crist Lock','Esse est.','Port Ernestinaport','Leonel Swift','DE40320706157508192298','PENDING',NULL,'2019-08-27 09:17:53','2019-08-27 09:17:53'),(17,'Akeem Schowalter','Leatha Torp Jr.','1-630-647-8902','Issac Orchard','220 Gudrun Hill Suite 639','Nisi sunt.','North Myamouth','Mr. Jerod Turner Sr.','DE07581053961966201546','PENDING',NULL,'2019-08-27 09:17:53','2019-08-27 09:17:53'),(18,'Danielle O\'Reilly','Wendell Robel','+1-396-735-8406','Fabian Stream','733 Keeling Route','Aliquam.','Mosciskiland','Junior Mosciski','DE80556236737829300231','PENDING',NULL,'2019-08-27 09:17:54','2019-08-27 09:17:54'),(19,'Mr. Charles Smitham I','Jakayla Labadie','502-387-6853','Karina Common','321 Sharon Villages','Quasi.','West Adelle','Ms. Jacky Johnston','DE14974604030965323763','PENDING',NULL,'2019-08-27 09:18:15','2019-08-27 09:18:15'),(20,'Henri O\'Reilly DDS','Blanche Paucek DDS','642.689.3613 x02061','Stanton Gateway','80698 Sauer Dam Suite 642','Inventore.','North Winston','Pansy Kerluke','DE71235446736124508386','PENDING',NULL,'2019-08-27 09:18:15','2019-08-27 09:18:15'),(21,'Makenna Bauch','Audie Mills','1-840-333-5353 x697','Manley Spurs','404 Bruen Greens','Dicta.','Denesikchester','Arnold Berge V','DE56456994309488832864','PENDING',NULL,'2019-08-27 09:18:16','2019-08-27 09:18:16'),(22,'Liman','Labaran','176105862','Persiaran Surian','B123','47810','Hamburg','Liman Adamu Labaran','12323','SUCCESS','a123357320092154c404c5cfa6ee49beee05eb88144b491b0170424c6ac156d05e6b0c00f3ebf64b8376fdedacb89e7c','2019-08-27 09:19:46','2019-08-27 09:19:47');
/*!40000 ALTER TABLE `customers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2019_08_27_110349_create_customers_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'wunder_db'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-08-28  1:20:24
